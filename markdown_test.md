#Safe in their Alabaster Chambers (216)#
###Emily Dickinson, 1830 - 1886###

Safe in their Alabaster Chambers  
untouched by Morning  
and untouched by Noon

Sleep the meek members of the Resurrection  
rafter of satin,  
and roof of stone.

Light laughs the breeze  
in her Castle above them  
babbles the bee in a stolid Ear,  
Pipe the Sweet Birds in ignorant cadence  
Ah, what sagacity perished here!

Grand go the Years in the Crescent above them  
Worlds scoop their Arcs  
And Firmaments row  
Diadems drop and Doges surrender  
Soundless as dots on a Disc of Snow  

